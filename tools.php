<div class="container">
    <div class="row col-12 d-flex align-items-center justify-content-center">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="d-flex justify-content-center">
              <a href="#"><img class="img-responsive" src="img/ipia_logo.png" alt="IPI Akademija"></a>
          </div> 
      </div>

      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h5>Studentska služba</h5>
        <p class="text-justify">Ova aplikacija Vam omogućuje da u nekoliko klikova naručite printanje uvjerenja o studiranju u studentskoj službi, koje možete preuzeti sutradan na istom mjestu.</p>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <a href="request.php" class="btn btn-primary btn-lg active">Zahtjev</a>
        <a href="logout.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Odjava</a>
    </div>
</div>
</div>