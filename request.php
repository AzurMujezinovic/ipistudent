<?php

include 'includes/header.php';


if (!isset($_SESSION["login"]))
    header("Location: login.php");

/*

$connect = new PDO("mysql:host = $host; dbname = $database", $dbuser, $password);
$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$query = "SELECT * FROM ipia_users.users WHERE index_nr = :index_nr";
			$statement = $connect->prepare($query);
			$statement->execute(
				array(
					'index_nr' => $_POST["index_nr"]	
				))
	
	
*/
?>

<div class="container req-form" style="width:500px;">
		<h3 class="podnaslov">Popuni zahtjev za potvrdu</h3>
		<form method="post" action="createpdf.php">
			<div class="md-form">
				<select class="form-control" id="st_program" name="st_program" required>			
				<option value="" selected disabled hidden>Studijski program</option>
					<option class="form-controler" value="Informacione Tehnologije">Informacione Tehnologije</option>
					<option class="form-controler" value="Tržišne Komunikacije">Tržišne Komunikacije</option>
					<option class="form-controler" value="Savremeno Poslovanje i Informatički Menadžment">Savremeno Poslovanje i Informatički Menadžment</option>
					<option class="form-controler" value="Informatika i Računarstvo">Informatika i Računarstvo</option>
					<option class="form-controler" value="Računovodsvo i Finansije">Računovodsvo i Finansije</option>
				</select>
				<small id="usernameHelp" class="form-text text-muted">Odaberite Vaš smjer</small>
			</div>



			
			<!-- <div class="md-form">
				<input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Vaše ime" required>
				<small id="usernameHelp" class="form-text text-muted">Unesite vaše ime.</small>
			</div>
			

			<div class="md-form">
				<input type="text" class="form-control" id="surname" name="surname" aria-describedby="surnameHelp" placeholder="Vaše prezime" required>
				<small id="surnameHelp" class="form-text text-muted">Unesite vaše prezime.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="index_nr" name="index_nr" aria-describedby="index_nrHelp" placeholder="Indeks je u formatu: I-XXXX/XX" required>
				<small id="index_nrHelp" class="form-text text-muted">Unesite vaš broj indeksa.</small>
			</div> 


			<div class="md-form">
				<select class="form-control" id="st_status" name="st_status" required>
					<option value="" selected disabled hidden>Odaberite Status</option>
					<option value="Redovan">Redovan</option>
					<option value="Vanredan">Vanredan</option>
				</select>
				<small id="st_statusHelp" class="form-text text-muted">Vaš status (redovan/vanredan).</small>
			</div> -->

			
			<div class="md-form">
				<select class="form-control" id="tip_potvrde" name="tip_potvrde" required>
					<option value="" selected disabled hidden>Tip potvrde</option>
					<option class="form-controler" value="zdravstvenog osiguranja">Potvrda za zdravstveno osiguranje</option>
					<option class="form-controler" value="stipendije">Potvrda za stipendiju</option>
					<option class="form-controler" value="bankovnog računa">Potvrda za račun u banci</option>
					<option class="form-controler" value="autobuske karte">Potvrda za autobusku kartu</option>
				</select>
				<small id="st_potvrdaHelp" class="form-text text-muted">Odaberite tip potvrde.</small>
			</div>

			<!--
			<div class="md-form">
				<input type="text" class="form-control datepicker" id="st_datumRodjenja" name="st_datumRodjenja" aria-describedby="st_datumRodjenjaHelp" placeholder="Datum rođenja" required>
				<small id="st_datumRodjenjaHelp" class="form-text text-muted">Odaberite vaš datum rođenja.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="st_mjestoRodjenja" name="st_mjestoRodjenja" aria-describedby="st_mjestoRodjenjaHelp" placeholder="Mjesto rođenja" required>
				<small id="st_mjestoRodjenjaHelp" class="form-text text-muted">Upišite mjesto rođenja.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="st_opcinaRodjenja" name="st_opcinaRodjenja" aria-describedby="st_opcinaRodjenjaHelp" placeholder="Općina rođenja" required>
				<small id="st_opcinaRodjenjaHelp" class="form-text text-muted">Općina u kojoj ste rođeni.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control drzave" id="st_drzavaRodjenja" name="st_drzavaRodjenja" aria-describedby="st_drzavaRodjenjaHelp" placeholder="Država rođenja" required>
				<small id="st_drzavaRodjenjaHelp" class="form-text text-muted">Država u kojoj ste rođeni.</small>
			</div>
			-->




				<div class="md-form">
				<select class="form-control" id="br_sem" name="br_sem" required>
					<option value="" selected disabled hidden>Odaberite semestar koji pohađate</option>
					<option class="form-controler" value="I">Prvi</option>
					<option class="form-controler" value="II">Drugi</option>
					<option class="form-controler" value="III">Treći</option>
					<option class="form-controler" value="IV">Četvrti</option>
					<option class="form-controler" value="V">Peti</option>
					<option class="form-controler" value="VI">Šesti</option>
				</select>
				<small id="br_semHelp" class="form-text text-muted">Broj Semestra</small>
			</div>



			<div class="md-form">
				<select class="form-control" id="br_upisa" name="br_upisa" required>
					<option value="" selected disabled hidden>Odaberite koliko puta ste upisali trenutni semestar</option>
					<option class="form-controler" value="I">Jedanput</option>
					<option class="form-controler" value="II">Dvaput</option>
					<option class="form-controler" value="III">Triput</option>
					<option class="form-controler" value="IV">Četiri puta</option>
					<option class="form-controler" value="V">Pet puta</option>
				</select>
				<small id="br_upisaHelp" class="form-text text-muted">Broj koliko ste puta upisali trenutni semestar</small>
			</div>
			
		<div class="card-body text-center">
			<input type="submit" name="request" class="btn btn-primary btn-lg active" value="Pošalji"/>
		</div>
			
		</form>
	</div>

<?php include 'includes/footer.php' ?>

