<?php 

include 'includes/header.php';

try
{
	$connect = new PDO("mysql:host = $host; dbname = $database", $dbuser, $password);
	$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	if(isset($_POST["login"]))
	{
		if(empty($_POST["index_nr"]) || empty($_POST["password"]))
		{
			$message = '<label class:"text-danger">Sva polja su potrebna!</label>';
		}
		else
		{
			$query = "SELECT * FROM ipia_users.users WHERE username = :username AND index_nr = :index_nr"; 
			$statement = $connect->prepare($query);
			$statement->execute(
				array(
					'username' => $_POST["username"],
					'index_nr' => $_POST["index_nr"]	
				)
			);
			$count = $statement->rowCount();
			$password = $_POST["password"];
			$hash = $statement->fetchColumn(11);
			if($count > 0 && password_verify($password, $hash))
			{
				$_SESSION["login"] = "1";
				$_SESSION["username"] = $_POST["username"];
				$_SESSION["index_nr"] = $_POST["index_nr"];
				$_SESSION["active"]   = "1";
				$msg = '<div id="notifikacija" class="card success-color text-center z-depth-2">
							<div class="card-body">
								<p class="white-text mb-0">Dobrodošli '.$_SESSION["username"].'!</p>
							</div>
        				</div>';
				$_SESSION["msg"] = $msg;
				header("location: index.php");
				exit();
			}
			else
			{
				$message = '<label class:"text-danger">Pogrešan unos, pokušajte ponovo!</label>';
			}
		}
	}
}
catch(PDOException $error)
{
	$message = $error->getMessage();
}

?>

<div class="container log-form" style="width:500px;">
	<?php 
	if(isset($message))
	{
		echo $message;
	}
	?>
	<h3 class="podnaslov">IPIA Prijava</h3>
	<form method="post" autocomplete="off">
		<div class="md-form">
			<i class="fa fa-user prefix grey-text"></i>
			<input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Vaše ime" required>
		</div>

		<div class="md-form">
			<i class="fa fa-file prefix grey-text"></i>
			<input type="text" class="form-control" id="index_nr" name="index_nr" aria-describedby="index_nrHelp" placeholder="Indeks je u formatu: I-XXXX/XX" required>
		</div>

		<div class="md-form">
			<i class="fa fa-lock prefix grey-text"></i>
			<input type="password" class="form-control" id="password" name="password" placeholder="Vaša lozinka">
		</div>

		<!--<div class="md-form">
			<i class="fa fa-exclamation-triangle prefix grey-text"></i>
			<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Potvrdite lozinku">
		</div>
		!-->
		<div class="card-body text-center">
		<input type="submit" name="login" class="btn btn-primary btn-lg active" value="Prijavi se"/>
		<a href="register.php" class="btn btn-primary btn-lg active">Registruj se</a>
	</div>
	</form>
</div>

<?php include 'includes/footer.php' ?>