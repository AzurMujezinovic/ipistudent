<?php 

$host 	  	= "localhost";
$username 	= "root";
$password 	= "";
$database 	= "ipia_users";
$message 	= "";

try
{
	$connect = new PDO("mysql:host = $host; dbname = $database", $username, $password);
	$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	if(isset($_POST["login"]))
	{
		if(empty($_POST["index_nr"]) || empty($_POST["password"]))
		{
			$message = '<label>All fields are required</label>';
		}
		else
		{
			 $query = "SELECT * FROM ipia_users.users WHERE username = :username AND password = :password";
			 $statement = $connect->prepare($query);
			 $statement->execute(
			 	array(
			 		'username' => $_POST["username"],
			 		'password' => $_POST["password"]
			 	)
			 );
			 $count = $statement->rowCount();
			 if($count > 0)
			 {
			 	$_SESSION["username"] = $_POST["username"];
			 	$_SESSION["surname"] = $_POST["surname"];
			 	$_SESSION["index_nr"] = $_POST["index_nr"];
			 	header("location:login_success.php");
			 }
			 else
			 {
			 	$message = '<label>Wrong Data</label>';
			 }
		}
	}
}
catch(PDOException $error)
{
	$message = $error->getMessage();
}


?>