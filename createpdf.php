<?php
include 'connection.php'; 
date_default_timezone_set('Europe/Belgrade');



$conn = new PDO("mysql:host = $host; dbname = $database", $dbuser, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$st_loginIndex = $_SESSION["index_nr"];


$sql = "SELECT * FROM ipia_users.users WHERE index_nr = '$st_loginIndex'";
foreach ($conn->query($sql) as $row) {
        $user_id            = $row['id'];
        $username 			    = $row['username'];
        $surname 			      = $row['surname'];
        $index_nr 			    = $row['index_nr'];
        $st_status			    = $row['st_status'];
        $st_datumrodjenja 	= $row['st_datumrodjenja'];
        $st_mjestorodjenja  = $row['st_mjestorodjenja'];
        $st_opcinarodjenja  = $row['st_opcinarodjenja'];
        $st_drzavarodjenja  = $row['st_drzavarodjenja'];
    }




////////// UPISAN N-ti put u N-ti semestar

			/*$st_program		= $_POST["st_program"];
			$datetime = new DateTime('tomorrow');
			$username 	= $_POST["username"];
			$surname 	= $_POST["surname"];
			$index_nr 	= $_POST["index_nr"];
			$stStatus   = $_POST["st_status"];
			$godRodj = $_POST ["st_datumRodjenja"];
			$datumRodj  = $_POST["st_datumRodjenja"];
			$mjestoRodj = $_POST["st_mjestoRodjenja"];
			$opcinaRodj = $_POST["st_opcinaRodjenja"];
			$drzavaRodj = $_POST["st_drzavaRodjenja"];*/
			$datetime = new DateTime('tomorrow');
			$st_program		= $_POST["st_program"];
			$tipPotv = $_POST ["tip_potvrde"];
			$brojUpisa = $_POST ["br_upisa"];
			$brojSem = $_POST ["br_sem"];

      $stmt = $conn->prepare("INSERT INTO ipia_users.potvrda SET tip_potvrde = ?, user_id = ?");
      $stmt->execute([$tipPotv, $user_id]);



$punoIme = $username. " " .$surname;
$godina_Rodj = substr($st_datumrodjenja, -3);
$dan_Rodj = substr ($st_datumrodjenja, 0, -5);

require('tfpdf.php');

$pdf = new tFPDF();
$pdf->AddPage('L', 'A5', 0);
$pdf->SetAutoPageBreak(TRUE, 0);
//$pdf->AddFont('Arial','','ArialSansCondensed.ttf',true);


if ($tipPotv == "zdravstvenog osiguranja")
	{
		$filename = $username . " " . $surname . " " . "potvrda za zdravstveno osiguranje" . '.pdf';
	}

elseif ($tipPotv == "stipendije")
	{
		$filename = $username . " " . $surname . " " . "potvrda za stipendiju" . '.pdf';
	}

elseif ($tipPotv == "bankovnog računa")
	{
		$filename = $username . " " . $surname . " " . "potvrda za banku" . '.pdf';
	}

else
	{
		$filename = $username . " " . $surname . " " . "potvrda za autobusku kartu" . '.pdf';
	}



$pdf -> SetXY(22, 48);
$pdf->SetFont('Arial','',11);
$pdf->Cell(70,10,$st_program, 0, 0, 'C');


$pdf -> SetXY(147, 48);
$pdf->Cell(40,10,$index_nr, 0, 0, 'C');


$pdf -> SetXY(30, 66.5);
$pdf->Cell(20,10,$datetime->format('d.m.'), 0, 0, 'C');

$pdf->SetFont('Arial','',9);
$pdf -> SetXY(59, 67);
$pdf->Cell(20,10,$datetime->format('y.'), 0, 0, 'C');

$pdf->SetFont('Arial','',11);
$pdf -> SetXY(34, 95);
$pdf->Cell(85,10,$username . " " . $surname, 0, 0, 'C');

$pdf -> SetXY(130, 95);
$pdf->Cell(38,10,$dan_Rodj, 0, 0, 'C');

$pdf->SetFont('Arial','',9);
$pdf -> SetXY(158, 96);
$pdf->Cell(38,10,$godina_Rodj, 0, 0, 'C');

$pdf->SetFont('Arial','',11);
$pdf -> SetXY(147, 103);
$pdf->Cell(42,10,$st_drzavarodjenja, 0, 0, 'C');

$pdf -> SetXY(25, 103);
$pdf->Cell(55,10,$st_mjestorodjenja, 0, 0, 'C');

$pdf -> SetXY(100, 103);
$pdf->Cell(35,10,$st_opcinarodjenja, 0, 0, 'C');

if ($st_status == "Redovan") 
{  
	$pdf -> SetXY(36, 113.5);
	$pdf->Cell(12,4, "", 1, 1, 'C');
}
else 
{
	$pdf -> SetXY(50, 113.5);
	$pdf->Cell(14, 4, "", 1, 1, 'C');
};


$pdf->SetFont('Arial','',10);
$pdf -> SetXY(91, 112);
$pdf->Cell(80,5,"Internacionalna poslovno-informaciona akademija", 0, 0, 'C');

$pdf->SetFont('Arial','',11);

$pdf -> SetXY(18, 120);
$pdf->Cell(20,5,$brojUpisa, 0, 0, 'C');

$pdf -> SetXY(55, 120);
$pdf->Cell(20,5,$brojSem, 0, 0, 'C');

$pdf->SetFont('Arial','',9);
$pdf -> SetXY(114, 120.5);       //////// OVDJE SE MIJENJA SKOLSKA GODINA
$pdf->Cell(20,5,'17', 0, 0, 'C');

$pdf -> SetXY(125, 120.5);              //////////////////////////// I OVDJE
$pdf->Cell(20,5,'18', 0, 0, 'C');

$pdf->SetFont('Arial','',11);
$pdf -> SetXY(100, 130);
$pdf->Cell(90,0,$tipPotv, 0, 0, 'C');

$pdf -> SetXY(22, 135);
$pdf->Cell(160,0,"------------------------------------------------------------------------------", 0, 0, 'C');


//$pdf->Output($filename, 'I');


// email stuff (change data below)
$to = "azurkos@hotmail.com";
$from = "ipiakademijatestnimail@gmail.com";
$subject = "Zahtjev za potvdu";

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// attachment name
$filename = $filename;

// encode data (puts attachment in proper format)
$pdfdoc = $pdf->Output("", "S");
$attachment = chunk_split(base64_encode($pdfdoc));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"charset=UTF-8";


// no more headers after this, we start the body! //

$body = "--".$separator.$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= "Stigao je zahtjev za potvrdu.".$eol;

/* message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;*/


// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;

// send message
mail($to, $subject, $body, $headers);

	
	

?>

<!DOCTYPE html>
<html>
<head>
	<title>IPIA Studentska Služba</title>
	<!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Custom styles -->
  <link href="css/ipipraksa.css" rel="stylesheet">

  <!-- Date Picker CSS -->
  <link href="css/default.css" rel="stylesheet" type="text/css">
  <link href="css/default.date.css" rel="stylesheet" type="text/css">
</head>
<body>
<!--
<nav class="navbar navbar-expand-lg navbar-dark info-color">
  <a class="navbar-brand" href="#"><img src="img/ipia_logo.png" alt="IPI Akademija" height="50px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Naslovna</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Akademija
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://ipi-akademija.ba/"><i class="fas fa-globe"></i> Oficijelna Stranica</a>
          <a class="dropdown-item" href="http://ipi-nastava.ba/moodle/"><i class="fas fa-graduation-cap"></i> DL Edukacijski sistem</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="https://www.facebook.com/ipiakademija/"><i class="fab fa-facebook-square"></i> Facebook</a>
          <a class="dropdown-item" href="https://www.instagram.com/ipi_akademija/"><i class="fab fa-instagram"></i> Instagram</a>
        </div>
      </li>
    </ul>
    
  </div>
</nav>
-->


<div class="container">
	<label class="text-danger"></label>

	<div class="row mtop-1 h-full align-items-center justify-content-center">
    <div class="col-12 d-flex justify-content-center align-items-center">
        <!--Card-->
        <!--Card-->
<div class="card">
    <h3 class="card-header navbar-dark info-color white-text text-center">Hvala na podnesenom zahtjevu!</h3>
    <div class="card-body">
        <h4 class="card-title text-center">Potvrdu možete preuzeti sutra u studentskoj službi IPI Akademije.</h4>
        <p class="card-text text-center">IPI Akademija</p>
        
  <div class="card-body text-center">
    <a href="index.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Naslovna </a>
    <a href="logout.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Odjava </a>
  </div>

    </div>
</div>
    </div>
</div>
</div>

<footer class="footer">
	<div class="container text-center">      
	<span class="text-center small-text text-muted">IPI Akademija | 2018</span>
	</div>
</footer>

<!-- Scripts (Keep this order!)-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="js/mdb.min.js"></script>
<script type="text/javascript" src="js/ipipraksa.js"></script>
<script type="text/javascript" src="js/picker.js"></script>
<script type="text/javascript" src="js/picker.date.js"></script>
<script type="text/javascript" src="js/jquery.easy-autocomplete.min.js"></script>

</body>
</html>

