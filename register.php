<?php

include 'includes/header.php';

try
{
	$connect = new PDO("mysql:host = $host; dbname = $database", $dbuser, $password);
	$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	if(isset($_POST["register"]))
	{
		if(empty($_POST["email"]) || empty($_POST["username"]) || empty($_POST["surname"]) || empty($_POST["index_nr"]) || empty($_POST["password"]))
		{
			$message = '<label class="text-danger">Sva polja su potrebna.</label>';
		}
		else
		{
			$email 		= $_POST["email"];
			$username 	= $_POST["username"];
			$surname 	= $_POST["surname"];
			$index_nr 	= $_POST["index_nr"];
			$maticni	= $_POST["st_jmbg"];
			$stStatus   = $_POST["st_status"];
			$datumRodj  = $_POST["st_datumRodjenja"];
			$mjestoRodj = $_POST["st_mjestoRodjenja"];
			$opcinaRodj = $_POST["st_opcinaRodjenja"];
			$drzavaRodj = $_POST["st_drzavaRodjenja"];
			$password 	= $_POST["password"];
			$hash 		= password_hash($password, PASSWORD_DEFAULT);

			/*$query = "SELECT * FROM ipia_users.users WHERE username = :username AND password = :password";
			$statement = $connect->prepare($query);*/

			$stmt2 = $connect->prepare("SELECT index_nr FROM ipia_users.users WHERE ? = index_nr");
			$params = array($_POST['index_nr']);
			$stmt2->execute($params);
			if ($stmt2->rowCount() > 0) {

				echo "<div class='p-3 mb-2 bg-danger text-white'>Korisnik sa brojem indeksa ". $index_nr ." već postoji, pokušajte ponovo.</div>";
			}
			else{
				echo "<div class='p-3 mb-2 bg-info text-white'>Hvala na registraciji!</div>";

				$stmt = $connect->prepare("INSERT INTO ipia_users.users SET st_email = ?, username = ?, surname = ?, index_nr = ?, st_jmbg = ?, st_status = ?, st_datumrodjenja = ?, st_mjestorodjenja = ?, st_opcinarodjenja = ?, st_drzavarodjenja = ?, password = ?");
				$stmt->execute([$email, $username, $surname, $index_nr, $maticni, $stStatus, $datumRodj, $mjestoRodj, $opcinaRodj, $drzavaRodj, $hash]);
				$msg = '<div id="notifikacija" class="card success-color text-center z-depth-2">
				<div class="card-body">
				<p class="white-text mb-0">Hvala na registraciji '.$username.'!</p>
				</div>
				</div>';
				$_SESSION["msg"] = $msg;
				$_SESSION['active'] = 0;


				// Send registration update to admin
				$to_2      = "azurkos@hotmail.com";
				$from = "ipiakademijatestnimail@gmail.com";
				$subject_2 = 'Account Registration for ('.$email.')';
				$message_body_2 = '
				Zdravo,

				Novi korisnik je registrovan na stranici za izdavanje potvrda.

				Korisnik je '.$username.' '.$surname.'.

				Dodatni podaci o korisniku:

				JMBG: '. $st_jmbg.'
				Broj Indeksa: '.$index_nr.'';

				//Ako želite odobriti ovu registraciju kliknite na link ispod:

				//http://localhost:4040/ipipraksa/verify.php?email='.$email.'&hash='.$hash; 

				mail( $to_2, $subject_2, $message_body_2 );
				
				//End mail

				header("location: index.php");
				exit();
			}
  		}
	
	}
}
catch(PDOException $error)
{
	$message = $error->getMessage();
}

?>

<div class="container reg-form" style="width:500px;">
		<h3 class="podnaslov" style = "padding-top: 10px;">IPIA Registracija</h3>
		<form method="post" autocomplete="off">
			<div class="md-form">
				<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Vaš kontakt mail" required>
				<small id="emailHelp" class="form-text text-muted">Unesite email koji koristite za komunikaciju.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Vaše ime" required>
				<small id="usernameHelp" class="form-text text-muted">Unesite vaše ime.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="surname" name="surname" aria-describedby="surnameHelp" placeholder="Vaše prezime" required>
				<small id="surnameHelp" class="form-text text-muted">Unesite vaše prezime.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="index_nr" name="index_nr" aria-describedby="index_nrHelp" placeholder="Indeks je u formatu: I-XXXX/XX" required>
				<small id="index_nrHelp" class="form-text text-muted">Unesite vaš broj indeksa.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="st_jmbg" name="st_jmbg" aria-describedby="st_jmbgHelp" placeholder="JMBG" required>
				<small id="st_jmbgHelp" class="form-text text-muted">Unesite vaš jedinstveni matični broj (13 znakova).</small>
			</div>

			<div class="md-form">
				<select class="form-control" id="st_status" name="st_status" required>
					<option value="" selected disabled hidden>Odaberite Status</option>
					<option class="form-controler" value="Redovan">Redovan</option>
					<option class="form-controler" value="Vanredan">Vanredan</option>
				</select>
				<small id="st_statusHelp" class="form-text text-muted">Vaš status (redovan/vanredan).</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control datepicker" id="st_datumRodjenja" name="st_datumRodjenja" aria-describedby="st_datumRodjenjaHelp" placeholder="Datum rođenja" required>
				<small id="st_datumRodjenjaHelp" class="form-text text-muted">Odaberite vaš datum rođenja.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="st_mjestoRodjenja" name="st_mjestoRodjenja" aria-describedby="st_mjestoRodjenjaHelp" placeholder="Mjesto rođenja" required>
				<small id="st_mjestoRodjenjaHelp" class="form-text text-muted">Upišite mjesto rođenja.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control" id="st_opcinaRodjenja" name="st_opcinaRodjenja" aria-describedby="st_opcinaRodjenjaHelp" placeholder="Općina rođenja" required>
				<small id="st_opcinaRodjenjaHelp" class="form-text text-muted">Općina u kojoj ste rođeni.</small>
			</div>

			<div class="md-form">
				<input type="text" class="form-control drzave" id="st_drzavaRodjenja" name="st_drzavaRodjenja" aria-describedby="st_drzavaRodjenjaHelp" placeholder="Država rođenja" required>
				<small id="st_drzavaRodjenjaHelp" class="form-text text-muted">Država u kojoj ste rođeni.</small>
			</div>

			<div class="md-form">
				<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Vaša lozinka">
			</div>

			<div class="md-form">
				<input type="password" class="form-control" id="password" name="password" placeholder="Potvrdite lozinku">
				<small id="message" class="form-text"></small>
			</div>
		<div class="card-body text-center">
			<input type="submit" name="register" class="btn btn-primary btn-lg active" value="Registruj se"/>
			<a href="login.php" class="btn btn-primary btn-lg active">Prijavi se</a>
		</div>
			<div class="pre-footer"></div>
		</form>
	</div>

<?php include 'includes/footer-reg.php' ?>

