
//Hide the login notification
setTimeout(function(){
    $("#notifikacija").fadeOut(400);
}, 3000)


//Test if password fields are the same
$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#password_confirm').val()) {
    $('#message').html('Lozinke su jednake!').css('color', '#3daf3d');
  } else {
    $('#message').html('Lozinke se ne podudaraju!').css('color', 'FF0000');
	}
});

//Date picker
$( document ).ready(function() {
	$('.datepicker').pickadate({
		today: 'Danas',
		clear: 'Izbriši',
		close: 'Zatvori',
		labelMonthNext: 'Prethodni mjesec',
		labelMonthPrev: 'Slijedeći mjesec',
		labelMonthSelect: 'Odaberite mjesec iz menija',
		labelYearSelect: 'Odaberite godinu iz menija',
		selectMonths: true,
		selectYears: 50,
		monthsFull: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Juli', 'August', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
		weekdaysShort: ['Pon', 'Uto', 'Sri', 'Čet', 'Pet', 'Sub', 'Ned'],
		format: 'dd.mm.yyyy.',
		formatSubmit: 'yyyy/mm/dd',
		max: true
	})
});

//Autocomplete countries
$( document ).ready(function() {
	var options = {

  url: "json/countries_BA.json",

  getValue: "ime",

  list: {	
    match: {
      enabled: true
    }
  },

  theme: "square"
};

$(".drzave").easyAutocomplete(options);

});