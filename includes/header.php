<?php 
include 'connection.php'; 
?>

<!DOCTYPE html>
<html>
<head>
	<title>IPIA Studentska Služba</title>
	<!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Custom styles -->
  <link href="css/ipipraksa.css" rel="stylesheet">

  <!-- Date Picker CSS -->
  <link href="css/default.css" rel="stylesheet" type="text/css">
  <link href="css/default.date.css" rel="stylesheet" type="text/css">
  
   
  
</head>
<body>
