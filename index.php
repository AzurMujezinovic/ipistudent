

<?php 

include 'includes/header.php';

?>
<div class="container-fluid pre-container">
	<p align="center">
		<a href="http://ipi-akademija.ba/"><i class="fas fa-globe fb-icon"></i> </a>
		<a href="http://ipi-nastava.ba/moodle/"><i class="fas fa-graduation-cap moodle-icon"></i> </a>
		<a href="https://www.facebook.com/ipiakademija/"><i class="fab fa-facebook-square fb-icon"></i></a>
		<a href="https://www.instagram.com/ipi_akademija/"><i class="fab fa-instagram ig-icon"></i></a>
	</p>
</div>
<div class="container-fluid main-container"> 

	<?php 
	/*
		if(isset($message))
		{
			echo '<label class="text-danger">'.$message.'</label>';
		}
		if(isset($_SESSION["msg"]))
		{
			echo $_SESSION["msg"];
		}
	*/
	?>

	<?php if((isset($_SESSION["login"]) && $_SESSION["login"] == "1") && $_SESSION["active"] == "1") 
	{
		 include 'tools.php';
	} 
	else 
	{
		include 'home-content.php';	
	}
	?>

</div>

<?php include 'includes/footer.php' ?>